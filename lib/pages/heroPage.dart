import 'package:flutter/material.dart';
import 'package:folk_dich/apis/getFolkngs.dart';
import 'package:folk_dich/models/folksSong.dart';
import 'package:folk_dich/widgets/folkContainer.dart';

const double pi = 3.1415926535897932;

class HeroPage extends StatefulWidget {
  HeroPage(this._title);

  final _title;

  @override
  _HeroPageState createState() => _HeroPageState();
}

class _HeroPageState extends State<HeroPage> {
  //data
  Future<List<FolkSong>> futureFolkAlbum;

  //dimensions
  final double _width = 270;
  final double _elevation = 4;

  @override
  void initState() {
    super.initState();
    futureFolkAlbum = fetchFolkData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: _width - 40,
              child: Hero(
                  tag: 'croquetas',
                  child: Image(image: AssetImage('assets/logos/logov2.jpg'))),
            ),
            SizedBox(height: 40),
            folkContainer(context, width: _width, futureList: futureFolkAlbum),
            SizedBox(height: 40),
            Hero(
              tag: 'buttonTag',
              child: SizedBox(
                width: _width,
                height: 50,
                child: RawMaterialButton(
                    elevation: _elevation,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    fillColor: Colors.green,
                    splashColor: Colors.lightBlueAccent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Transform.rotate(
                          angle: pi,
                          child: Icon(Icons.details,
                              textDirection: TextDirection.rtl,
                              color: Colors.white,
                              size: 25),
                        ),
                        SizedBox(width: 10),
                        Text("get Back!",
                            style: TextStyle(color: Colors.white, fontSize: 19))
                      ],
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/');
                    }),
              ),
            ),
          ],
        )));
  }
}
