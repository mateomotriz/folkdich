import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage(this._title);

  final _title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  //dimensions
  final double _width = 235;
  final double _elevation = 4;

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Card(
                margin: EdgeInsets.only(bottom: 40),
                elevation: 4,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(28)),
                child: Container(
                    width: _width,
                    child: Padding(
                        padding: EdgeInsets.all(15),
                        child: Image(
                          image: AssetImage('assets/triskel.png'),
                          //image: AssetImage('assets/logos/logov1'),
                        )))),
            Hero(
              tag: 'buttonTag',
              child: SizedBox(
                width: _width,
                height: 50,
                child: RawMaterialButton(
                    elevation: _elevation,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    fillColor: Colors.blueAccent,
                    splashColor: Colors.lightBlueAccent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.details,
                            textDirection: TextDirection.rtl,
                            color: Colors.white,
                            size: 25),
                        SizedBox(width: 10),
                        Text("Ride like the wind",
                            style: TextStyle(color: Colors.white, fontSize: 19))
                      ],
                    ),
                    onPressed: () {
                      //Navigator.pushNamed(context, 'login');
                      Navigator.pushNamed(context, '/folk');
                    }),
              ),
            ),
            SizedBox(height: 40),
            Container(
              width: _width - 40,
              child: Hero(
                  tag: 'croquetas',
                  child: Image(
                    image: AssetImage('assets/logos/logov2.jpg'),
                  )),
            )
          ],
        )));
  }
}
