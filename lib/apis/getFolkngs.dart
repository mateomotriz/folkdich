import 'dart:convert';
import 'package:folk_dich/models/folksSong.dart';
import 'package:http/http.dart' as http;

Future<List<FolkSong>> fetchFolkData() async {
  //credits to jsongenerator.com
  List<FolkSong> folkList = [];
  var jsonData = await http
      .get('https://next.json-generator.com/api/json/get/41tFuDg_u?indent=2');

  // http returns 200 if everything is ok
  if (jsonData.statusCode == 200) {
    // let's read the dynamic list
    for (var i in json.decode(jsonData.body)) {
      folkList.add(FolkSong.fromJson(i));
    }
    return folkList;
  } else {
    print("unable to retrieve data");
    //throw Exception('Viele Problems');
  }
}