import 'package:flutter/material.dart';
import 'pages/heroPage.dart';
import 'pages/loginPage.dart';

class FolkDich extends StatelessWidget {
  @override
  var _title = 'Folk Dich';

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      theme: ThemeData(primaryColor: Colors.lightGreenAccent),
      //home: LoginPage(_title),
      
      //navigation
      initialRoute: '/',
      routes: {
        '/': (context) => LoginPage(_title), 
        '/folk': (context) => HeroPage(_title), 
      }
    );
  }
}
