import 'package:flutter/material.dart';
import 'package:folk_dich/models/folksSong.dart';

Widget folkContainer(BuildContext context,
    {double width = 200, Future<List<FolkSong>> futureList}) {
  Color _borderColor = Colors.black38;

  return Container(
      height: 250,
      width: width,
      decoration: BoxDecoration(
        border: Border.all(
          color: _borderColor,
          width: 2,
        ),
      ),
      child: FutureBuilder(
        future: futureList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Material(
              child: ListView.builder(
                  // general attributes
                  padding: EdgeInsets.only(top: 4),
                  scrollDirection: Axis.vertical,
                  // builder stuff
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int i) {
                    return songsContainer(snapshot.data[i].song,
                        snapshot.data[i].artist, snapshot.data[i].cover);
                  }),
            );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          // by default
          return Center(
            child: SizedBox(
                width: 80,
                child: LinearProgressIndicator(
                    backgroundColor: Colors.greenAccent)),
          );
        },
      ));
}

Container songsContainer(String _songName, String _songArtist, String _imageURL,
    {Color borderColor = Colors.black38}) {
  double _containerHeight = 61.5;

  return Container(
      padding: EdgeInsets.all(5),
      alignment: Alignment.centerLeft,
      height: _containerHeight,
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: borderColor, width: 1))),
      child: Row(
        children: <Widget>[
          Flexible(
              flex: 2,
              child: SizedBox(
                  height: _containerHeight,
                  width: _containerHeight,
                  child: SizedBox(
                      child: Image(
                          fit: BoxFit.cover, image: NetworkImage(_imageURL))))),
          Flexible(
            flex: 8,
            child: Padding(
              padding: EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(_songName,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                  Text(
                    _songArtist,
                    style: TextStyle(fontStyle: FontStyle.italic),
                  )
                ],
              ),
            ),
          )
        ],
      ));
}
