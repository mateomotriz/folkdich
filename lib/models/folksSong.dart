class FolkSong {
  final int id;
  final String song;
  final String artist;
  final String cover;

  FolkSong({this.id, this.song, this.artist, this.cover});

  factory FolkSong.fromJson(Map<String, dynamic> json){
    return FolkSong(
      id: json['id'],
      song: json['song'],
      artist: json['artist'],
      cover: json['cover']
    );
  }
}
